export const ERR_UNNATURAL_NUM =
  'Number of winnable cards,TicketStats#count, must be a natural number, i.e. from 1, 2, ..'

export const ERR_LESS_THAN_1 = "max must be greater or equal to 1."

export const ERR_NOT_INTEGER = "max arg must be of type Integer, Floats or Double are invalid."
