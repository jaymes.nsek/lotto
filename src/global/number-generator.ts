import {PayTable} from "../app/game/global/interfaces";
import {ERR_LESS_THAN_1, ERR_NOT_INTEGER, ERR_UNNATURAL_NUM} from "./number-generator.constants";


export class NumberGenerator {

  /**
   * Targeted at symbols between 1 and 9 (inclusive), with 0 being used to indicate a lose.
   *
   * @param pt
   * @param numCards
   * @throws RangeError if {@link PayTable#count} exceeds the boundary 1 - 9. TODO test
   */
  createDeck_sym0to9(pt: PayTable, numCards: number): Uint8Array {
    let finalArr: Uint8Array = new Uint8Array()
    let arr: Uint8Array
    let numWinCards = 0 // Tally the total number of winnable cards for this PayTable

    let start
    let count
    for (let stats of pt) {
      if ((count = stats.count) < 1)
        throw new Error(ERR_UNNATURAL_NUM)

      start = 0
      let end = start + count

      // fill a new array, with the sym, with elements totalling the number of cards
      arr = new Uint8Array(stats.count)
      arr.fill(stats.sym, start, end)

      // Combine the new arr with the previously generated ones
      finalArr = Uint8Array.from([...finalArr, ...arr])
      numWinCards += stats.count // Update num of winning cards
    }

    const possibleLose = numCards - numWinCards

    // TODO Generally, the goal is to throw the error when possibleLose is negative
    //  however, catching possible errors, where the ratio is not at least, say, 70% loss
    //  is also helpful.
    /*if (((possibleLose/numCards) * 100) < 70)
      throw new Error("Minimum win/lose ratio not met!")*/
    // Replace with the above before test, this element will need to extracted and mocked,
    // To allow testing other conditions that do not satisfy it.

    // Generate the total count of possible loses with 0's, and combine to existing struct
    arr = new Uint8Array(possibleLose)
    finalArr = Uint8Array.from([...finalArr, ...arr])
    return finalArr
  }

  /**
   * Return a random integer, between the default min of 0 and the max.
   */
  randInt(max: number) {
    if (max <= 0)
      throw new Error(ERR_LESS_THAN_1)

    if(!Number.isInteger(max))
      throw new Error(ERR_NOT_INTEGER)

    const min = 0
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  /**
   * @param arr the array to shuffle in-place.
   * @note based on the Fisher–Yates shuffle algorithm
   */
  shuffle(arr: Uint8Array): Uint8Array {
    let swapIndex, iVal

    for (let i = arr.length - 1; i > 0; i--) {
      swapIndex = this.randInt(i)
      // Swap
      iVal = arr[i]
      arr[i] = arr[swapIndex]
      arr[swapIndex] = iVal
    }

    return arr
  }
}
