import {NumberGenerator} from "./number-generator";
import {PayTable} from "../app/game/global/interfaces";
import {ERR_LESS_THAN_1, ERR_UNNATURAL_NUM} from "./number-generator.constants";
import {NUM_OF_CARDS, payTable2Euro} from "../app/game/global/constants/paytable.constants";


describe('NumberGenerator', () => {
  let gen = new NumberGenerator()

  beforeAll(() => {
    gen = new NumberGenerator()
  })

  describe('#createDeck_sym0to9', () => {
    // count totals 15
    let payTableTest: PayTable

    beforeEach(() => {
      payTableTest = [
        {sym: 9, count: 1, prize: 1000},
        {sym: 8, count: 1, prize: 200},
        {sym: 7, count: 1, prize: 40},
        {sym: 6, count: 1, prize: 20},
        {sym: 5, count: 2, prize: 5},
        {sym: 4, count: 2, prize: 1},
        {sym: 3, count: 2, prize: 0.50},
        {sym: 2, count: 2, prize: 0.20},
        {sym: 1, count: 3, prize: 0.10}
      ]
    })

    it('should return the expected array for valid PayTable', () => {
      // possibleLose = 25 - payTableCount = 10
      let deck = gen.createDeck_sym0to9(payTableTest, 25) //payTable10Cent

      expect(deck.length).toEqual(25)
      expect(deck).toEqual(
        new Uint8Array([9, 8, 7, 6, 5, 5, 4, 4, 3, 3, 2, 2, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
      )
    })

    it('should throw Error if any value of PayTable[i]#count is not a Natural-Number', () => {
      // Change index 3, sym 6, count to 0
      payTableTest[3].count = 0
      expect(payTableTest[3]).toEqual({sym: 6, count: 0, prize: 20})

      // The possibleLose = 25 - payTableCount = 10
      expect(function () {
        gen.createDeck_sym0to9(payTableTest, 25)
      }).toThrowError(Error, ERR_UNNATURAL_NUM)
    })
  })

  describe('#randInt', () => {
    describe('min and max ceiling', () => {
      const len = 20
      const randValues: Array<number> = new Array(len)

      beforeEach(() => {
        // Call 20 time and store the value
        for (let i = 0; i < len; i++)
          randValues[i] = gen.randInt(1)
      })

      it('should span the min and max celling [0, 1]', () => {
        // statistically, the 20 values should contain  0's and 1's every time
        expect(randValues).toContain(0)
        expect(randValues).toContain(1)
      })
    })

    it('should return a number between 0 and max', () => {
      const max = 50000
      const integer = gen.randInt(max)

      expect(integer).toBeLessThanOrEqual(max)
      expect(integer).toBeGreaterThanOrEqual(0)
    })

    it('should throw Error if max == 0', () => {
      expect(function () {
        gen.randInt(0)
      })
        .toThrowError(Error, ERR_LESS_THAN_1)
    })

    it('should throw Error if max < 0', () => {
      expect(function () {
        gen.randInt(-1)
      })
        .toThrowError(Error, ERR_LESS_THAN_1)
    })
  })

  describe('#shuffle', () => {
    describe('with arr.len > 1', () => {
      const params = [
        {desc: 'should shuffle the array', input: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]}
      ]

      params.forEach(({desc, input}) => {
        it(desc, () => {
          // Clone the array here so it is unreferenced
          const arr = new Uint8Array([...input])

          const shuffledArr = gen.shuffle(arr)
          expect(shuffledArr).not.toEqual(new Uint8Array(input))

          // check all values in the original array are in return array
          expect(shuffledArr.sort()).toEqual(new Uint8Array(input))
        })
      })
    })

    describe('with "empty" or "len==1" input', () => {
      const params = [
        {desc: 'should return [value] for len==1', input: [1]},
        {desc: 'should return [] for empty array', input: []}
      ]

      params.forEach(({desc, input}) => {
        it(desc, () => {
          // Clone the array here so it is unreferenced
          const shuffledArr = new Uint8Array([...input])

          expect(gen.shuffle(shuffledArr))
            .toEqual(new Uint8Array(input))
        })
      })
    })
  })

  // Isolating each dependency is not beneficial, here, the test is mainly
  // one of interaction at the product size of 50,000 elements = 50,000bytes
  it('should generate a sorted deck in-line with production conditions', () => {
    const deck = gen.createDeck_sym0to9(payTable2Euro, NUM_OF_CARDS)
    expect(deck).toHaveSize(50000)

    const shuffledDec = gen.shuffle(new Uint8Array([...deck]))
    expect(shuffledDec).toHaveSize(50000)

    const selectedIndex = gen.randInt(NUM_OF_CARDS)
    expect(selectedIndex).toBeGreaterThanOrEqual(0)
    expect(selectedIndex).toBeLessThanOrEqual(50000)

    // Confirm that the values still only contain 0 - 9
    function isBetween0And9(value: number) {
      const flag = (9 - value)
      return flag >= 0 && flag <= 9
    }
    expect(shuffledDec.every(isBetween0And9)).toBeTrue()

    const sym = deck[selectedIndex]
    expect(isBetween0And9(sym)).toBeTrue()
  })
})
