import {BetHistory} from "../../app/game/global/interfaces";


export class BetHistoryQueue {
  private static DEFAULT_MAX_LEN = 5
  private MAX_LEN = 2
  private histories: BetHistory[]

  /**
   * @param max The maximum allowed length of the Queue, after which stale items
   * atop are shifted out in favour of recent ones. If less than 2, then the default
   * value of 5 is used.
   */
  constructor(max: number = BetHistoryQueue.DEFAULT_MAX_LEN) {
    this.MAX_LEN = max < 2 ? BetHistoryQueue.DEFAULT_MAX_LEN : max
    this.histories = []
  }

  /**
   * Add the latest data to index 0, the end of the queue
   */
  add(history: BetHistory) {
    // start del item
    this.histories.splice(0, 0, history)
    this.performFIFO()
  }

  getData(): BetHistory[] {
    return [...this.histories]
  }

  get length() {
    return this.histories.length
  }

  /**
   * Shift out the top-stale history, if {@link MAX_LEN} is reached
   */
  private performFIFO() {
    const len = this.histories.length
    if (len < this.MAX_LEN)
      return;

    this.histories.splice(this.MAX_LEN, len - this.MAX_LEN)
  }
}
