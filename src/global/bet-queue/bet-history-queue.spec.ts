import {BetHistoryQueue} from "./bet-history-queue";

describe('BetQueue', () => {

  it('should shift out older items once max size is reached', () => {
    let data
    const MAX_SIZE = 3
    const queue = new BetHistoryQueue(MAX_SIZE)
    const history = {
      isWin: false,
      symbols: [8, 8, 8, 8, 8, 8, 8, 8, 8],
      prize: '£22',
      matchedSym: 0,
      betValue: '£0.25',
      time: "2022-06-12"
    }

    // 1st Add: Add history with deref object
    queue.add({...history})

    data = queue.getData()
    expect(queue.length).toBe(1)
    expect(data[0].symbols).toEqual([8, 8, 8, 8, 8, 8, 8, 8, 8])

    // 2nd Add: Modify symbols and add with deref object
    history.symbols = [2, 2, 2, 2, 2, 2, 2, 2, 2]
    queue.add({...history})

    // Confirm that the position is such that oldest items are at the end
    data = queue.getData()
    expect(queue.length).toBe(2)
    expect(data[0].symbols).toEqual([2, 2, 2, 2, 2, 2, 2, 2, 2])
    expect(data[1].symbols).toEqual([8, 8, 8, 8, 8, 8, 8, 8, 8])

    // 3rd Add: Modify symbols and add with deref object
    history.symbols = [6, 6, 6, 6, 6, 6, 6, 6, 6]
    queue.add({...history})

    data = queue.getData()
    expect(queue.length).toBe(MAX_SIZE)
    expect(data[0].symbols).toEqual([6, 6, 6, 6, 6, 6, 6, 6, 6])
    expect(data[1].symbols).toEqual([2, 2, 2, 2, 2, 2, 2, 2, 2])
    expect(data[2].symbols).toEqual([8, 8, 8, 8, 8, 8, 8, 8, 8])

    // Fourth Add:
    history.symbols = [1, 1, 1, 1, 1, 1, 1, 1, 1]
    queue.add({...history})

    // Confirm that the first Add is pushed out, and only the 2nd and 3rd exist
    // and that size MAX_SIZE is not exceeded
    data = queue.getData()
    expect(queue.length).toBe(MAX_SIZE)
    expect(data[0].symbols).toEqual([1, 1, 1, 1, 1, 1, 1, 1, 1])
    expect(data[1].symbols).toEqual([6, 6, 6, 6, 6, 6, 6, 6, 6])
    expect(data[2].symbols).toEqual([2, 2, 2, 2, 2, 2, 2, 2, 2])
  })
})
