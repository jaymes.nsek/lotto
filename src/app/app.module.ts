import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CardComponent} from './game/card/card.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from "@angular/forms";
import {GameComponent} from './game/game.component';
import {WinDialog} from "./game/global/modals/win/win-dialog.component";
import {WagerService} from "./game/global/services/wager.service";
import {AppMatModule} from "./app-mat.module";
import {BetInfoDialog} from "./game/global/modals/bet-info/bet-info-dialog.component";


@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    CardComponent,
    WinDialog,
    BetInfoDialog
  ],
  imports: [
    AppMatModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
  ],
  providers: [WagerService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
