import {MatDialogModule} from "@angular/material/dialog";
import {MatIconModule} from "@angular/material/icon";
import {NgModule} from "@angular/core";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  imports: [
    MatDialogModule,
    MatIconModule,
    MatButtonToggleModule,
    MatButtonModule
  ],
  exports: [
    MatDialogModule,
    MatIconModule,
    MatButtonToggleModule,
    MatButtonModule
  ]
})
export class AppMatModule { }
