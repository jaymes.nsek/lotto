import {Component, OnInit} from '@angular/core';
import {User} from "./global/interfaces";
import {WagerService} from "./global/services/wager.service";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  // dummy User instance
  user!: User

  constructor(private wagerService: WagerService) {
  }

  ngOnInit(): void {
    // When game launches, assume user token is retrieved here,
    // pass object to CardComponent, via @Input
    this.wagerService.getUser().then((token => {
        this.user = token
      })
    )
  }
}
