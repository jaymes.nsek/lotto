import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DrawSym, User} from "../global/interfaces";
import {WagerService} from "../global/services/wager.service";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {WinDialog} from "../global/modals/win/win-dialog.component";
import {CardPresenter} from "./card.component.presenter";
import {BetInfoDialog} from "../global/modals/bet-info/bet-info-dialog.component";
import {BetInfoType} from "../global/enums/global";


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnChanges {
  @Input() user: User | null = null

  presenter!: CardPresenter

  symbols: any[][]
  pickedSym: DrawSym|0 = 0 // Used in template to flag which symbols to highlight as win

  balance: number | undefined = 0
  isWagering: boolean = false // To prevent multiple clicks to the backend

  matDialogRef: MatDialogRef<WinDialog> | null = null

  constructor(private wagerService: WagerService, private dialog: MatDialog,) {
    this.presenter = new CardPresenter(wagerService)
    this.symbols = this.presenter.intervalSymbols

  }

  ngOnInit(): void {
    this.attachLis()
    this.balance = this.user?.balance.getBalance()
  }

  ngOnChanges(changes: SimpleChanges) {
    // Since user is delivered as async, it's necessary to reflect this
    // in case the value is not yet init in ngOnInit
    const userChange = changes["user"]
    if (userChange)
      this.balance = this.user?.balance.getBalance()
  }

  attachLis() {
    // Align AR on first load
    window.addEventListener('load', () => {
      this.alignAspectRatio()

    })

    // Align AR on screen resize
    window.addEventListener('resize', () => {
      this.alignAspectRatio()
    })
  }

  /**
   * Matches the width and height of the root <ul> to an aspect-ratio of 1/1.
   */
  alignAspectRatio() {
    const card = <HTMLElement>document.getElementById('card-container')
    const cardWidth = card?.offsetWidth
    card.style.height = String(cardWidth + 'px')
  }

  //region Wager methods and its helpers

  onWager() {
    if (this.isWagering)
      return

    // Set as first think in case other calls are blocking
    this.isWagering = true

    // Balance is insufficient - indicate to user that account requires top-up
    const betValue = Number(this.presenter.betVal)
    if (!this.user?.balance.hasEnough(betValue)) {
      console.warn("onWager: Insufficient balance!")
      this.isWagering = false
      return
    }

    // Subtract the wager fee from user current balance so deduction is immediately reflected
    //  and not sometime in the future.
    this.balance = this.user.balance.getBalance() - Number(this.presenter.betVal)

    this.wagerService.wager(this.user, this.presenter.betVal)
      .then(result => {
        //console.warn("onWager: wagerService returned with -> ", result.valueOf())

        const newSymbols = this.presenter.getSymMap(result.symbols)

        // TODO test case: Handle case where server incorrectly returns outside
        //  of the {@link Symbol} set - and thus null is returned
        if (!newSymbols) {
          console.error("Symbols array does not conform to type DrawSym")
          return;
        }
        this.symbols = newSymbols
        this.pickedSym = result.isWin ? result.winSymbol : 0

        // Adopt new User object and reapply the new balance; if wager was successful it should
        // be the same as the artificially updated computation, before call to backend
        this.user = result.user
        this.balance = this.user?.balance.getBalance()

        // Show win dialog if user has won
        if (result.isWin)
          this.openOutcomeDialog(result.prize)

        // Re-enable wagering
        this.isWagering = false
      }).catch(/*err => {

    }*/)
  }

  //endregion

  isWinSym(sym: number) {
    const flag = (sym === this.pickedSym) ? 'win' : ''
    console.warn(" ======= isWinSym: ", flag, " [", this.pickedSym, sym, "]")
    return flag
  }

  //region Dialogs

  /**
   * Open dialog; reapply {@link intervalSymbols} after it is closed.
   */
  openOutcomeDialog(prize: string, isWin: boolean = true) {
    this.dialog.open(WinDialog, {
      panelClass: 'app-win-dialog',
      width: '90%',
      maxWidth: '400px',
      data: {isWin, prize},
    })
    /*.afterClosed().subscribe(() => {
      this.symbols = this.presenter.intervalSymbols
    }
  )*/
  }

  showBetInfoDialog(type: BetInfoType) {
    const data = this.presenter.makeBetInfoDialogData(type)

    this.dialog.open(BetInfoDialog, {
      panelClass: 'app-bet-info-dialog',
      width: '90%',
      height: '90%',
      maxWidth: '600px',
      maxHeight: '430px',
      data
    });
  }

  //endregion
}
