import {CardPresenter} from "./card.component.presenter";
import {WagerService} from "../global/services/wager.service";


describe('Card Component Presenter', () => {
  let presenter: CardPresenter

  beforeAll(() => {
    // TODO - Mock Service as required
    const wagerService: WagerService = new WagerService()
    presenter = new CardPresenter(wagerService)
  })

  it('should return valid symbols', () => {
    expect(presenter.validSymArr).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9])
  })

  describe('isValidSymbols ' , () => {
    const param = (desc: string, symbols: number [], expected: boolean) => ({desc, symbols, expected})

    const params = [
      param('should return true for subsets of Symbol AND size is 9',
        [1, 3, 2, 8, 9, 7, 5, 4, 6], true),
      param('should return false for subsets of Symbol BUT size != 9',
        [1, 3, 2, 8, 9, 7, 5, 4], false),
      param('should return false if all elements are NOT subsets of Symbol BUT size is 9',
        [1, 3, 28, 8, 99, 7, 5, 4, 6], false),
      param('should return false if all elements are NOT subsets of Symbol AND size size != 9',
        [10, 8, 87, 7, 5, 4, 0], false)
    ]

    params.forEach(({desc, symbols, expected}) => {
      it(`${desc} [[${symbols}], ${symbols.length}]`, () => {
        expect(presenter.isValidSymbols(symbols)).toEqual(expected)
      })
    })
  })

  describe('getSymMap', () => {
    const param = (desc: string, input: any, spyVal: boolean, expected: any) => ({desc, input, spyVal, expected})

    const params = [
      param('should return "null" for non-conformance to #isValidSymbols which results in FALSE',
        [1, 3, 44, 8, 79, 7, 5, 4, 6], false,null),
      param('should return "number[][]" for non-conformance to #isValidSymbols which results in TRUE',
        [8, 9, 7, 1, 3, 2, 5, 4, 6], true,[[8, 9, 7], [1, 3, 2], [5, 4, 6]])
    ]

    params.forEach(({desc, input, spyVal, expected}) => {
      it(desc, () => {
        spyOn(presenter, 'isValidSymbols').and.returnValue(spyVal)

        const symbols = presenter.getSymMap(input)
        expect(symbols).toEqual(expected)

        // If the return is an array, then it should be a 3 x 3 array
        if (Array.isArray(symbols)) {
          expect(symbols).toHaveSize(3)
          expect(symbols[0]).toHaveSize(3)
          expect(symbols[1]).toHaveSize(3)
          expect(symbols[2]).toHaveSize(3)
        }

        expect(presenter.isValidSymbols).toHaveBeenCalled()
      })
    })
  })
})
