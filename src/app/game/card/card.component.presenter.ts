import {BetHistory, IntervalSym, PayTable, DrawSym} from "../global/interfaces";
import {BetValue} from "../global/enums/betvalue";
import {BetInfoType} from "../global/enums/global";
import {WagerService} from "../global/services/wager.service";
import {BetInfoData} from "../global/modals/bet-info/bet-info-dialog.component";
import {Observable} from "rxjs";
import {getPayTable} from "../global/constants/paytable.constants";

export class CardPresenter {
  validSymArr: DrawSym[] = [1, 2, 3, 4, 5, 6, 7, 8, 9]

  // Shown in-between game play when no symbols are present
  intervalSymbols: IntervalSym[][] = [
    ['?', '?', '?'],
    ['?', '?', '?'],
    ['?', '?', '?']
  ]
  betVal: BetValue
  payTable: PayTable

  // Track changes so previous data is initially made available before update arrives
  // also if too large, consider storing in local or session Storage
  private latestBetHistory: BetHistory[] = []

  private $betHistory!: Observable<BetHistory[]>

  BetInfoType = BetInfoType // Expose Enum to Template

  constructor(private wagerService: WagerService) {
    // Set default values
    this.betVal = BetValue.EURO_1
    this.payTable = getPayTable(this.betVal)
  }

  /**
   * Return array of values from {@link BetValue}.
   */
  get betValues() {
    return [BetValue.CENT_10, BetValue.CENT_25, BetValue.CENT_50, BetValue.EURO_1, BetValue.EURO_2]
  }

  isValidSymbols(symbols: number[]): boolean {
    if (symbols.length !== 9)
      return false

    for (let sym of symbols)
      if (!this.validSymArr.includes(<DrawSym>sym))
        return false

    return true
  }

  /**
   * Map valid symbols array to a 2D 3x3 map.
   */
  getSymMap(symbols: number[]): number[][] | null {
    if (!this.isValidSymbols(symbols))
      return null;

    return [
      [symbols[0], symbols[1], symbols[2]],
      [symbols[3], symbols[4], symbols[5]],
      [symbols[6], symbols[7], symbols[8]],
    ]
  }

  onBetValueChange() {
    // Update with applicable payTable onBetValueChange
    this.payTable = getPayTable(this.betVal)
  }

  /**
   * Return current {@link BetHistory} state synchronously via Observable; and push updates
   * when they resolved;
   *
   * @note this reflects the fact call is async - however, since the current betHistory is a
   * subset of that on server, it is delivered synchronously.
   */
  get betHistory(): Observable<BetHistory[]> {
    if (this.$betHistory)
      return this.$betHistory

    this.$betHistory = new Observable<BetHistory[]>((subscriber) => {
        try {
          subscriber.next(this.latestBetHistory)

          this.wagerService.getBetHistory().then((result => {
            // TODO validate that result is of BetHistory
            this.latestBetHistory = result
            subscriber.next(this.latestBetHistory)
            subscriber.complete()
          }))
        } catch (err) {
          subscriber.error(err)
        }
      }
    )

    return this.$betHistory
  }

  /**
   * Return the current data, required when opening {@link BetInfoDialog}
   */
  makeBetInfoDialogData(type: BetInfoType): BetInfoData {
    return {
      type,
      betHistory: this.betHistory,
      betValue: this.betVal,
      payTable: this.payTable
    }
  }
}
