import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GameComponent } from './game.component';
import {RouterTestingModule} from "@angular/router/testing";
import {AppMatModule} from "../app-mat.module";
import {CardComponent} from "./card/card.component";
import {FormsModule} from "@angular/forms";


describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GameComponent,
        CardComponent
      ],
      imports: [
        RouterTestingModule,
        AppMatModule,
        FormsModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
