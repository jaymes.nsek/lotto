/**
 * Targeted at frontend, where an attempt to modify the object
 * would render the token invalid, for this reason, #deduct is omitted.
 */
export class Balance {
  protected balance!: number

  public constructor(balance: number) {
    this.balance = balance
  }

  getBalance(): number {
    return this.balance
  }

  hasEnough(betValue: number):boolean {
    const postState = this.getBalance() - betValue
    return postState >= 0
  }
}

/**
 * Used, in the simulated backend, to deduct the bet value from a
 * user's current balance.
 */
export class BackendBalance extends Balance {
  /**
   * @param value the value to deduct from the account balance
   * @returns the new balance if deduction was successful
   */
  deduct(value: number): Promise<number> {
    return new Promise(((resolve, reject) => {
      setTimeout(() => {
        if(!this.hasEnough(value))
          return reject('Insufficient funds!');

        this.balance = BackendBalance.to2dp(this.balance - value)
        return resolve(this.balance);
      }, 500)
    }))
  }

  /**
   * Enforce two decimal places
   */
  private static to2dp(value: number) {
    let newVal = Number.parseFloat(String(value)).toFixed(2)
    return Number.parseFloat(newVal)
  }
}
