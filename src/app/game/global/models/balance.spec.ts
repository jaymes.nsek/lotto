import {BackendBalance, Balance} from "./balance";
import {fakeAsync, tick} from "@angular/core/testing";


describe('Balance', () => {

  describe('#hasEnough', () => {
    let balanceObj: Balance

    const falseDesc = 'should reject if balance is insufficient against bet value'
    const trueDesc = 'should accept if wager request would not result in negative balance'
    const params = [
      {desc: falseDesc, balance: 1, bet: 2.0, expected: false},
      {desc: falseDesc, balance: 0.245, bet: 0.25, expected: false},
      {desc: falseDesc, balance: 0.248, bet: 0.25, expected: false},
      {desc: trueDesc, balance: 100, bet: 1.0, expected: true},
      {desc: trueDesc, balance: 0.51, bet: 0.5, expected: true},
    ]

    params.forEach(({desc, balance, bet, expected}) => {
      it(`${desc} [${balance}, ${bet}]`, () => {
        balanceObj = new Balance(balance)
        expect(balanceObj.getBalance()).toEqual(balance)

        expect(balanceObj.hasEnough(bet)).toEqual(expected)
      })
    })
  })
})

describe('BackendBalance', () => {

  describe('#deduct', () => {
    let backendBalance: BackendBalance

    describe('Balance is insufficient to fulfil bet value', () => {
      const desc = 'should reject request with err msg'
      const expected = 'Insufficient funds!' // rejectMsg

      const params = [
        {desc, balance: 1, bet: 2.0, expected},
        {desc, balance: 0.245, bet: 0.25, expected},
        {desc, balance: 0.248, bet: 0.25, expected}
      ]

      params.forEach(({desc, balance, bet, expected}) => {
        it(`${desc} [${balance}, ${bet}]`, fakeAsync(() => {
          let newBalance, error
          backendBalance = new BackendBalance(balance)

          expect(backendBalance.getBalance()).toBe(balance)

          backendBalance.deduct(bet)
            .then(result => {
              newBalance = result
            })
            .catch(err => {
              error = err
            })

          tick(500) // This is set to the current simulated delay in #deduct

          expect(newBalance).toBeUndefined()
          expect(error).toMatch(expected)
        }))
      })
    })

    describe('Wager request would not result in negative balance', () => {
      const desc = 'should honour request and return updated balance'

      const params = [
        {desc, balance: 100, bet: 1.0, expected: 99},
        {desc, balance: 0.51, bet: 0.5, expected: 0.01},
      ]

      params.forEach(({desc, balance, bet, expected}) => {
        it(`${desc} [${balance}, ${bet}]`, fakeAsync(() => {
          let newBalance, error
          backendBalance = new BackendBalance(balance)

          expect(backendBalance.getBalance()).toBe(balance)

          backendBalance.deduct(bet)
            .then(result => {
              newBalance = result
            })
            .catch(err => {
              error = err
            })

          tick(500) // This is set to the current simulated delay in #deduct

          // @ts-ignore
          expect(newBalance).toBe(expected)
          expect(error).toBeUndefined()
        }))
      })
    })
  })
})
