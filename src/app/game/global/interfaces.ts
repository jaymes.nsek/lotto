import {Balance} from "./models/balance";


export type DrawSym = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9

export type IntervalSym = '?'

export interface User {
  balance: Balance
}

export interface BetData {
  isWin: boolean
  symbols: number[]
  prize: string
}

/**
 * @param winningSymbol when zero indicates that there is no win, no sym in symbols should
 * occur more than twice, and the hasWon flag should also be set to false. When 1 - 9
 * (inclusive) it indicates wager was won, there should be 3x pairs in the symbols array,
 * and hasWon == true.
 */
export interface BetResult extends BetData {
  user: User
  winSymbol: DrawSym | 0
}

/**
 * @param betValue use String; To allow addition of currency, plus ensure
 * there is no need for conversion between currencies, which would be at
 * the mercy of inflation
 * @param time use String rather than date, to avoid unnecessary re-computations
 * @param prize use String ; To allow addition of currency
 */
export interface BetHistory extends BetData {
  matchedSym: number
  betValue: String
  time: string
}

export type TicketStats = { sym: DrawSym, count: number, prize: number }

export type PayTable = [
  TicketStats, TicketStats, TicketStats,
  TicketStats, TicketStats, TicketStats,
  TicketStats, TicketStats, TicketStats
]
