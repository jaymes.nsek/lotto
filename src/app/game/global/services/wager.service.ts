import {Injectable} from '@angular/core';
import {BetHistory, BetResult, User, DrawSym, PayTable} from "../interfaces";
import {BetValue} from "../enums/betvalue";
import {BackendBalance} from "../models/balance";
import {NumberGenerator} from "../../../../global/number-generator";
import {getPayTable, NUM_OF_CARDS} from "../constants/paytable.constants";
import {BetHistoryQueue} from "../../../../global/bet-queue/bet-history-queue";

type UserDetails = {
  10097653: { balance: BackendBalance, betHistory: BetHistoryQueue }
}

const ERR_DRAW_SYM = (val: any) =>
  `DrawSym error: symbol must be 0 - 9 (inclusive), but a value of ${val} was returned.`

const prependEuro = (val: any) => `€${val}`


/**
 * Class contains dummy methods to help simulate frontend interactions
 * with the backend.
 */
@Injectable({
  providedIn: 'root'
})
export class WagerService {

  private users!: UserDetails
  private numGen: NumberGenerator

  constructor() {
    this.generateUsers()
    this.numGen = new NumberGenerator()
  }

  private generateUsers() {
    // Where "10097653" is taken as the unique userID
    this.users = {
      10097653: {
        balance: new BackendBalance(100),
        betHistory: new BetHistoryQueue(20)
      }
    }
  }

  /**
   * Return the dummy user object
   */
  getUser(): Promise<User> {
    return Promise.resolve({
      balance: this.users["10097653"].balance
    })
  }

  wager(user: User, bet: BetValue): Promise<BetResult> {
    const balance: BackendBalance = this.users["10097653"].balance

    return new Promise((resolve, reject) => {
      balance.deduct(Number(bet)).then(() => {
        // The user has enough balance to satisfy the bet
        const sym = this.pick(bet)
        const symbols = this.padSymbol(<DrawSym>sym)

        let betResult: BetResult
        const user: User = {balance}

        const prizeEuro = prependEuro(
          this.getPrize(bet, getPayTable(bet), sym)
        )

        // Primed for a sym == 0 case
        betResult = {
          user,
          isWin: false,
          winSymbol: <DrawSym>sym,
          symbols,
          prize: prizeEuro
        }

        if ((sym >= 0) && (sym <= 9)) {
          betResult.isWin = (sym !== 0)
        } else {
          throw new Error(ERR_DRAW_SYM(sym))
        }

        // Update bet history
        this.addBetHistory(bet, symbols, sym, betResult.isWin, prizeEuro)

        resolve(betResult)
      }).catch(err => {
        reject(err ?? "Wager request failed.")
      })
    })
  }

  /**
   * Returns an integer between 0 and 9 (inclusive), where 0 indicates a losing ticket.
   *
   * @note the value is amplified to 3x when 1 - 9 and represents the symbol.
   */
  pick(bet: BetValue): number {
    const payTable = getPayTable(bet)
    let deck: Uint8Array = this.numGen.createDeck_sym0to9(payTable, NUM_OF_CARDS)

    // Shuffle the deck three times
    for (let i = 0; i < 3; i++)
      deck = this.numGen.shuffle(deck)

    // Match rand int to corresponding element for symbol
    const picked = this.numGen.randInt(NUM_OF_CARDS)
    return deck[picked]
  }

  /**
   * Generate six random numbers, to go with 3x sym arg if the sym arg is
   * 1 and 9 (inclusive), or nine random numbers if sym arg is 0.
   *
   * The generator satisfies the condition that each symbol features 0, 1 or 2 times,
   * in the resulting array; except it is 1 ... 9 - in which, exactly 3 times.
   *
   * @param sym a number between 0 and 9 (inclusive)
   * @return number[] array containing 9 elements; 6 random symbols to go with the sym,
   * if arg is 1 ... 9 (inclusive), i.e. a win; or 9 random symbols if the sym arg is
   * a 0, i.e. a lost condition;
   */
  padSymbol(sym: DrawSym | 0): number[] {
    const FULL = 2

    // Init tracker array and Flag that the indices 0 and that corresponding to the
    //  picked sym, if not also 0, have reached their max count
    const tracker = [
      FULL, 0, 0, 0, 0,
      0, 0, 0, 0, 0
    ]
    tracker[sym] = FULL

    const symbols: DrawSym[] = []

    // If the sym is not 0, then add 3 instances of it to symbols array
    if (sym !== 0)
      symbols.push(sym, sym, sym)

    // break, shuffle and return once size == 9 is reached.
    let value
    do {
      value = this.numGen.randInt(9)

      // check tracker arr before adding to ensure it hasn't reached its limit
      if (tracker[value] == FULL)
        continue

      symbols.push(<DrawSym>value)
      tracker[value] = tracker[value] + 1
    } while (symbols.length < 9);

    const shuffledSymbols = this.numGen.shuffle(new Uint8Array(symbols))
    return Array.from(shuffledSymbols)
  }

  /**
   * @param bet
   * @param payTable
   * @param sym should be a positive integer between 1 and 9 (inclusive)
   * @return the prize matching the sym on the {@link PayTable}, or 0,
   * if the sym arg is 0 or not a member of the symbol set.
   */
  getPrize(bet: BetValue, payTable: PayTable, sym: number): number {
    const NO_PRIZE = 0
    if (sym < 1 || sym > 9)
      return NO_PRIZE

    let prize = 0
    for (let row of payTable)
      if (row.sym === sym) {
        prize = row.prize
        break;
      }

    return prize
  }

  private addBetHistory(bet: BetValue,
                        symbols: number[],
                        matchedSym: number,
                        isWin: boolean,
                        prize: string) {
    this.users["10097653"].betHistory.add({
      isWin,
      matchedSym,
      betValue: prependEuro(bet),
      symbols,
      time: this.timestamp(),
      prize: prize
    })
  }

  private timestamp(): string {
    const d = new Date()

    const getMonth = (date: Date) => {
      const month = date.getMonth() + 1
      return (month <= 9) ? `0${month}` : String(month)
    }

    //"2022-06-22  15:00:31 ${}"
    return `${d.getFullYear()}-${getMonth(d)}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`
  }

  getBetHistory(): Promise<BetHistory[]> {
    // Return dynamic dummy data
    return Promise.resolve(
      [...this.users["10097653"].betHistory.getData()]
    )
  }
}
