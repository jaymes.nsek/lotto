import {TestBed} from '@angular/core/testing'
import {WagerService} from './wager.service'
import {BetValue} from "../enums/betvalue";
import {DrawSym} from "../interfaces";
import {
  payTable10Cent,
  payTable1Euro,
  payTable25Cent,
  payTable2Euro,
  payTable50Cent
} from "../constants/paytable.constants";

describe('WagerService', () => {
  let service: WagerService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(WagerService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  describe('#pick', () => {
    const params = [BetValue.CENT_10, BetValue.CENT_50, BetValue.EURO_2]

    params.forEach((betValue) => {
      it(`should return a random number between 1 - 9 inclusive for €${betValue}`, () => {
        const sym = service.pick(betValue)
        expect(sym).toBeGreaterThanOrEqual(0)
        expect(sym).toBeLessThanOrEqual(9)
      })
    })
  })

  describe('#padSymbol', () => {
    let symbols

    describe('with sym == 0', () => {
      it('should have all numbers occurring <= 2 time', () => {
        symbols = service.padSymbol(0)
        expect(symbols).toHaveSize(9)

        // Sort array in asc order to set up flagging that no value occurs more than twice
        symbols = symbols.sort()
        let numStr = symbols.join('')

        // Check that none of the sym exceed their max limit
        expect(numStr).not.toMatch(/1{3,}|2{3,}|3{3,}|4{3,}|5{3,}|6{3,}|7{3,}|8{3,}|9{3,}/g)
      })
    })

    describe('with 1 <= sym <= 9 ', () => {
      const params = [
        {winSym: 1, negRegEx: /1{4,}|2{3,}|3{3,}|4{3,}|5{3,}|6{3,}|7{3,}|8{3,}|9{3,}/g, posRegEx: /1{3}/g},
        {winSym: 4, negRegEx: /1{3,}|2{3,}|3{3,}|4{4,}|5{3,}|6{3,}|7{3,}|8{3,}|9{3,}/g, posRegEx: /4{3}/g},
        {winSym: 9, negRegEx: /1{3,}|2{3,}|3{3,}|4{3,}|5{3,}|6{3,}|7{3,}|8{3,}|9{4,}/g, posRegEx: /9{3}/g},
      ]

      params.forEach(({winSym, negRegEx, posRegEx}) => {
        it(`should have 3x the winning sym and upto 2 times each of others [${winSym}]`, () => {
          symbols = service.padSymbol(<DrawSym> winSym)
          expect(symbols).toHaveSize(9)

          symbols = symbols.sort()
          let numStr = symbols.join('') // Example string after sorting and join: "111257889"

          // First check that none of the sym exceed their max limit
          expect(numStr).not.toMatch(negRegEx)
          // Confirm that the winning sym of "1" occurs 3x
          expect(numStr).toMatch(posRegEx)
        })
      })
    })
  })

  describe('#getPrize', () => {

    describe('prize-sym match', () => {
      const params = [
        {betValue: BetValue.EURO_2, payTable: payTable2Euro, sym: 9, expected: 20000},
        {betValue: BetValue.EURO_1, payTable: payTable1Euro, sym: 8, expected: 2000},
        {betValue: BetValue.CENT_50, payTable: payTable50Cent, sym: 4, expected: 5},
        {betValue: BetValue.CENT_25, payTable: payTable25Cent, sym: 2, expected: 0.5},
        {betValue: BetValue.CENT_10, payTable: payTable10Cent, sym: 1, expected: 0.10},
      ]

      params.forEach(({betValue, payTable, sym, expected}) => {
        it(`should return the corresponding prize of sym [${sym}, ${expected}]`,  () => {
          const prize = service.getPrize(betValue, payTable, sym)
          expect(prize).toBe(expected)
        })
      })
    })

    describe('prize-sym MISMATCH', () => {
      const params = [
        {desc: 'a "negative float"', sym: -21.333},
        {desc: 'a "float"', sym: 1.1},
        {desc: '"0"', sym: 0},
        {desc: '"not a member of Symbol set"', sym: 27},
      ]

      params.forEach(({desc, sym}) => {
        it(`should return 0 if "sym" arg is ${desc} [${sym}]`,  () => {
          // For these tests, there is no need to diversify BetValue and PayTable
          // as the ##getPrize is expected to return before the for loop
          const prize = service.getPrize(BetValue.EURO_1, payTable1Euro, sym)
          expect(prize).toBe(0)
        })
      })
    })
  })

})
