/**
 * To reduce risk of applying inaccurate value in code
 *
 * @note String Enum necessary so Euros is in fractional notation as per specification
 */
export enum BetValue {
  CENT_10 = '0.1',
  CENT_25 = '0.25',
  CENT_50 = '0.5',
  EURO_1 = '1.0',
  EURO_2 = '2.0'
}
