import {PayTable} from "../interfaces";
import {BetValue} from "../enums/betvalue";


export const NUM_OF_CARDS = 50000

export function getPayTable(value: BetValue) {
  switch (value) {
    case BetValue.CENT_10:
      return payTable10Cent
    case BetValue.CENT_25:
      return payTable25Cent
    case BetValue.CENT_50:
      return payTable50Cent
    case BetValue.EURO_1:
      return payTable1Euro
    case BetValue.EURO_2:
      return payTable2Euro
  }
}

/**
 * 9 elements in total. Array entries are from the lowest symbol (match 1s)
 * to the highest (match 9s).
 */
export const payTable10Cent: PayTable = [
  {sym: 9, count: 1, prize: 1000},
  {sym: 8, count: 4, prize: 200},
  {sym: 7, count: 10, prize: 40},
  {sym: 6, count: 25, prize: 20},
  {sym: 5, count: 50, prize: 5},
  {sym: 4, count: 200, prize: 1},
  {sym: 3, count: 400, prize: 0.50},
  {sym: 2, count: 2000, prize: 0.20},
  {sym: 1, count: 10000, prize: 0.10}
]

export const payTable25Cent: PayTable = [
  {sym: 9, count: 1, prize: 2500},
  {sym: 8, count: 4, prize: 500},
  {sym: 7, count: 10, prize: 100},
  {sym: 6, count: 25, prize: 50},
  {sym: 5, count: 50, prize: 12.5},
  {sym: 4, count: 200, prize: 2.5},
  {sym: 3, count: 400, prize: 1.25},
  {sym: 2, count: 2000, prize: 0.5},
  {sym: 1, count: 10000, prize: 0.25}
]

export const payTable50Cent: PayTable = [
  {sym: 9, count: 1, prize: 5000},
  {sym: 8, count: 4, prize: 1000},
  {sym: 7, count: 10, prize: 200},
  {sym: 6, count: 25, prize: 100},
  {sym: 5, count: 50, prize: 25},
  {sym: 4, count: 200, prize: 5},
  {sym: 3, count: 400, prize: 2.5},
  {sym: 2, count: 2000, prize: 1},
  {sym: 1, count: 10000, prize: 0.5}
]

export const payTable1Euro: PayTable = [
  {sym: 9, count: 1, prize: 10000},
  {sym: 8, count: 4, prize: 2000},
  {sym: 7, count: 10, prize: 400},
  {sym: 6, count: 25, prize: 200},
  {sym: 5, count: 50, prize: 50},
  {sym: 4, count: 200, prize: 10},
  {sym: 3, count: 400, prize: 5},
  {sym: 2, count: 2000, prize: 2},
  {sym: 1, count: 10000, prize: 1}
]

export const payTable2Euro: PayTable = [
  {sym: 9, count: 1, prize: 20000},
  {sym: 8, count: 4, prize: 4000},
  {sym: 7, count: 10, prize: 800},
  {sym: 6, count: 25, prize: 400},
  {sym: 5, count: 50, prize: 100},
  {sym: 4, count: 200, prize: 20},
  {sym: 3, count: 400, prize: 10},
  {sym: 2, count: 2000, prize: 4},
  {sym: 1, count: 10000, prize: 2}
]
