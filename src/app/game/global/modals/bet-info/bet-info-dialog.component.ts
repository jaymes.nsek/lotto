import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {BetHistory, PayTable} from "../../interfaces";
import {BetValue} from "../../enums/betvalue";
import {BetInfoType} from "../../enums/global";
import {Observable} from "rxjs";

export type BetInfoData = {
  type: BetInfoType,
  payTable: PayTable,
  betValue: BetValue,
  betHistory: Observable<BetHistory[]>
}

/**
 * @note For the {@link BetInfoData}, if either {@link PayTable} and {@link BetValue}
 * is present, then the other should also be supplied as they are used in the
 * same context.
 */
@Component({
  selector: 'app-bet-info-dialog',
  templateUrl: './bet-info-dialog.component.html',
  styleUrls: ['./bet-info-dialog.component.css']
})
export class BetInfoDialog implements OnInit {

  BetInfoType // Expose the type to template
  betHistories: BetHistory[] = []

  footerNote = '* Based on a theoretical series of 50,000 cards, where each' +
    ' bet has the same chance to win.'

  constructor(public dialogRef: MatDialogRef<BetInfoDialog>,
              @Inject(MAT_DIALOG_DATA) public data: BetInfoData) {
    this.BetInfoType = BetInfoType
  }

  ngOnInit() {
    // subscribe to BetHistory updates
    this.data.betHistory.subscribe((betHistories => {
      this.betHistories = betHistories
    }))
  }

  onCloseClicked(): void {
    this.dialogRef.close();
  }

  get isBetHistory() {
    return this.data.type === BetInfoType.BET_HISTORY
  }

  get table(): PayTable | [] {
    const payTable = this.data.payTable
    return !!payTable ? payTable : []
  }

  getMatchInfo(bh: BetHistory): string {
    return bh && bh.isWin ? `You Won ${bh.prize}` : 'No match'
  }

  togglePayTable() {
    this.data.type = BetInfoType.PAY_TABLE
  }

  toggleBetHistory() {
    this.data.type = BetInfoType.BET_HISTORY
  }
}
