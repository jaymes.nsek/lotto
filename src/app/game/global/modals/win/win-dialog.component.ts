import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-win-dialog',
  templateUrl: './win-dialog.component.html',
  styleUrls: ['./win-dialog.component.css']
})
export class WinDialog {
  constructor(public dialogRef: MatDialogRef<WinDialog>,
              @Inject(MAT_DIALOG_DATA) public data: {isWin: boolean, prize: string}) {
  }

  onCloseClicked(): void {
    this.dialogRef.close();
  }

  get winMsg() {
    return `You ${(this.data.isWin ? 'Won' : 'Lost')}: ${this.data.prize}`
  }
}
